#nom = input('Entrez votre nom :')
#print("bonjour",nom,"!")

# répétition 5 fois, boucle for
#nom = input('Entrez votre nom :')
#for i in range(5):
#	print("bonjour", nom, "!")
#	print(i)

#Adapter le code précédent pour afficher tous les nombres de 1 à 100.
for i in range(100):
	print(i+1)

#Afficher un rectangle de taille 4x5 composé de "x".
rectangle = "x" * 5 
for i in range(4):
	print(rectangle)

#Afficher tous les nombres de à 100 à 1.
for i in range(-100):
	print(i+1)